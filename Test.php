<?php

class Test {

    public function __construct($count) {
        $mysqli = @new mysqli("localhost", "root", "9338922", "lv2_2");
        if ($mysqli->connect_errno) {
            die('Connect Error: ' . $mysqli->connect_errno);
            exit();
        }
        $this->createMyISAM($mysqli);
        $this->createInnoDB($mysqli);
        $this->fill($mysqli, $count);
    }

    private function createMyISAM($mysqli) {
        $createInfo = "CREATE TABLE `info` (
                     `id` int(11) NOT NULL auto_increment,
                     `name` varchar(255) default NULL,
                     `desc` text default NULL,
                      PRIMARY KEY (`id`)
                     ) ENGINE=MyISAM DEFAULT CHARSET=cp1251;";

        $createData = "CREATE TABLE `data` (
                      `id` int(11) NOT NULL auto_increment,
                      `date` date default NULL,
                      `value` INT(11) default NULL,
                      PRIMARY KEY (`id`)
                      ) ENGINE=MyISAM DEFAULT CHARSET=cp1251;";

        $createLinc = "CREATE TABLE `link` (
                     `data_id` int(11) NOT NULL,
                     `info_id` int(11) NOT NULL
                     ) ENGINE=MyISAM DEFAULT CHARSET=cp1251;";


        $mysqli->query($createInfo);
        $mysqli->query($createData);
        $mysqli->query($createLinc);
    }

    private function createInnoDB($mysqli) {
        $createInfo = "CREATE TABLE IF NOT EXISTS `info_innodb` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `name` varchar(255) DEFAULT NULL,
                      `desc` text,
                       PRIMARY KEY (`id`)
                       ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $createData = "CREATE TABLE IF NOT EXISTS `data_innodb` (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                       `date` date DEFAULT NULL,
                       `value` int(11) DEFAULT NULL,
                        PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $createLinc = "CREATE TABLE IF NOT EXISTS `link_innodb` (
                       `data_id` int(11) NOT NULL,
                       `info_id` int(11) NOT NULL,
                        KEY `FK_link_innodb_data_innodb` (`data_id`),
                        KEY `FK_link_innodb_info_innodb` (`info_id`),
                        CONSTRAINT `FK_link_innodb_data_innodb` FOREIGN KEY (`data_id`) REFERENCES `data_innodb` (`id`),
                        CONSTRAINT `FK_link_innodb_info_innodb` FOREIGN KEY (`info_id`) REFERENCES `info_innodb` (`id`)
                        )ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $mysqli->query($createInfo);
        $mysqli->query($createData);
        $mysqli->query($createLinc);
    }

    private function fill($mysqli, $count) {
        $stmtInfo = $mysqli->prepare("INSERT INTO info (name,`desc`) VALUES (?,?)");
        $stmtInfo->bind_param('ss', $name, $desc);
        $stmtInfoInnoDB = $mysqli->prepare("INSERT INTO info_innodb (name,`desc`) VALUES (?,?)");
        $stmtInfoInnoDB->bind_param('ss', $name, $desc);


        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        for ($i = 0; $i < $count; $i++) {
            $randomName = '';
            for ($y = 0; $y < 5; $y++) {
                $randomName .= $characters[rand(0, $charactersLength - 1)];
            }
            $name = $randomName;
            $randomDesc = '';
            for ($y = 0; $y < 5; $y++) {
                $randomDesc .= $characters[rand(0, $charactersLength - 1)];
            }
            $desc = $randomDesc;
            $stmtInfo->execute();
            $stmtInfoInnoDB->execute();
        }
        $stmtInfo->close();
        $stmtInfoInnoDB->close();

        $stmtData = $mysqli->prepare("INSERT INTO data (date, value) VALUES (NOW(), ?)");
        $stmtData->bind_param('i', $value);
        $stmtDataInnoDB = $mysqli->prepare("INSERT INTO data_innodb (date, value) VALUES (NOW(), ?)");
        $stmtDataInnoDB->bind_param('i', $value);
        for ($i = 0; $i < $count; $i++) {
            $value = rand();
            $stmtData->execute();
            $stmtDataInnoDB->execute();
        }
        $stmtData->close();
        $stmtDataInnoDB->close();

        $stmtLink = $mysqli->prepare("INSERT INTO link (data_id, info_id) VALUES (?,?)");
        $stmtLink->bind_param('ii', $dataId, $infoId);
        $stmtLinkInnoDB = $mysqli->prepare("INSERT INTO link_innodb (data_id, info_id) VALUES (?,?)");
        $stmtLinkInnoDB->bind_param('ii', $dataId, $infoId);
        for ($i = 0; $i < $count; $i++) {

            $dataId = rand(1, $count);
            $infoId = rand(1, $count);
            $stmtLink->execute();
            $stmtLinkInnoDB->execute();
        }
        $stmtLink->close();
        $stmtLinkInnoDB->close();
    }

    public function getMyISAM() {
        $mysqli = new mysqli("localhost", "root", "9338922", "lv2_2");
        $defaultQuery = "select * from data,link,info where link.info_id = info.id and link.data_id = data.id";

        if ($result = $mysqli->query($defaultQuery)) {
            $rowArray = array();
            while ($row = $result->fetch_assoc()) {
                $rowArray[] = $row;
            }
            $result->free();
        }
        $mysqli->close();
    }

    public function getInnoDB() {
        $mysqli = new mysqli("localhost", "root", "9338922", "lv2_2");
        $defaultQuery = "select data_innoDB.id, data_innoDB.date, data_innoDB.value, info_innoDB.id,info_innodb.name, info_innodb.`desc`
                        from link_innodb
                        LEFT JOIN data_innoDB ON link_innodb.data_id =data_innodb.id
                        LEFT JOIN info_innodb ON link_innodb.info_id =info_innodb.id;";

        if ($result = $mysqli->query($defaultQuery)) {
            $rowArray = array();

            while ($row = $result->fetch_assoc()) {
                $rowArray[] = $row;
            }
            $result->free();
        }
        $mysqli->close();
    }

}
